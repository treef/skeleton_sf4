# Skeleton SF4

## Projet

C'est un projet qui permet d'avoir un squellette d'une appli SF4

## Install

Clone 
> git clone https://gitlab.com/treef/skeleton_sf4.git
Via HTTP

> git clone git@gitlab.com:treef/skeleton_sf4.git
Via SSH

Vendor 

>composer install    
Crée fichier vendor avec les différents module du composer.json 

>composer update 
Mettre à jour les module via le composer.json

>composer clear-cache
Delete les caches

>composer diagnose
Diagnostique du composer actuel

>composer self-update
Met un jour le composer

[![composer](https://d1q6f0aelx0por.cloudfront.net/product-logos/composer.png)](https://getcomposer.org/)

MySql 

BDD 1 Default
>php bin/console doctrine:database:create

>php bin/console doctrine:schema:update --force

BDD 2 statistique

>php bin/console doctrine:database:create --connection=statistique

>php bin/console doctrine:schema:update --force --em=statistique

##Utilisation BDD

Utilisation du multi base 

**A venir** 