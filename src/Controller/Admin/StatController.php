<?php
/**
 * Created by PhpStorm.
 * User: tcornado
 * Date: 28/02/2019
 * Time: 10:58
 */

namespace App\Controller\Admin;

use App\Entity\Main\User;
use App\Entity\Statistique\Stat;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/** @Route("/admin") */
class StatController extends AbstractController
{

    /** @var ObjectManager */
    private $entityManager;

    /** @var UserRepository */
    private $userRepository;

    /** @var EngineInterface */
    private $templating;

    public function __construct(RegistryInterface $registry, EngineInterface $templating)
    {
        $manager = $registry->getManagerForClass(User::class);
        $this->userRepository = $manager->getRepository(User::class);

        $this->templating = $templating;
    }

    /**
     * @Route("/", name="admin")
     */
    public function countProduct():Response
    {
        $sql = $this->userRepository->createQueryBuilder('u')
            ->select('count(u.id)');
        $count_user= $sql
            ->getQuery()->getResult();

        $stat = new Stat();
        $stat->setCountUser($count_user[0][1]);

        return $this->render('admin/index.html.twig',
            ['mainNavAdmin' => true,
                'title' => 'Espace Admin',
                'stat' => $stat
            ]
        );
    }
}