<?php
/**
 * Created by PhpStorm.
 * User: tcornado
 * Date: 28/02/2019
 * Time: 17:49
 */

namespace App\Controller\Admin\User;

use App\Entity\Main\User;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;


use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/** @Route("/admin") */
class DeleteUserController extends AbstractController
{

    /** @var UserRepository */
    private $userRepository;

    /** @var EngineInterface */
    private $templating;

    public function __construct(RegistryInterface $registry, EngineInterface $templating)
    {
        $manager = $registry->getManagerForClass(User::class);
        $this->userRepository = $manager->getRepository(User::class);
        $this->templating = $templating;
    }

    /**
     * @Route("/user/{user}", name="deleteUser")
     */
    public function __invoke(User $user):Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($user);
        $entityManager->flush();

        return $this->redirectToRoute('manageUser');
    }
}
