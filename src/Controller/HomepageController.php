<?php
namespace App\Controller;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController {

    /** @var EngineInterface */
    private $templating;


    public function __construct(RegistryInterface $registry, EngineInterface $templating)
    {
        $this->templating = $templating;

    }


    /**
     * @Route("/", name="homepage")
     */
    public function index(Request $request) {

        return new Response($this->templating->render('homepage/index.html.twig', [
            'mainNavHome'=>true,
            'title'=>'Accueil',
        ]));
    }
}