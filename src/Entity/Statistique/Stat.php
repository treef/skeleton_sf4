<?php
namespace App\Entity\Statistique;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="stat")
 * @UniqueEntity(fields="email")
 * @ORM\Entity()
 */
class Stat
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    private $countUser;

    /**
     * @return mixed
     */
    public function getCountUser()
    {
        return $this->countUser;
    }

    /**
     * @param mixed $countUser
     */
    public function setCountUser($countUser): void
    {
        $this->countUser = $countUser;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
}